# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi


# Put your fun stuff here.
#
# Private shell settings
#

#######################################################################
# aliases to be safe
#######################################################################
alias ls='ls -F --color=auto'
alias ll='ls -l'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias grep='grep --color=auto'
alias suspend='sudo loginctl suspend'

#######################################################################
# colors!! the most important, right?
#######################################################################
export CLICOLOR=1

#export TERM="xterm-256color"
if [ "x$DISPLAY" != "x" ]; then
	export HAS_256_COLORS=yes
	alias tmux="tmux -2"
	if [ "$TERM" = "xterm" ]
	then
		export TERM=xterm-256color
	fi
else
	if [ "$TERM" == "xterm" ] || [ "$TERM" == "xterm-256color" ]
	then
		export HAS_256_COLORS=yes
		alias tmux="tmux -2"
	fi
fi

if [ "$TERM" = "screen" ] && [ "$HAS_256_COLORS" = "yes" ]; then
	export TERM=screen-256color
fi


#######################################################################
# default editor for every use
#######################################################################
export EDITOR=/usr/bin/vim
export LESS="-F -X $LESS"	# no paging if less than a page


#######################################################################
# npm
#######################################################################
NPM_PACKAGES="${HOME}/.npm-packages"
export PATH="$PATH:$NPM_PACKAGES/bin"

# Preserve MANPATH if you already defined it somewhere in your config.
# Otherwise, fall back to `manpath` so we can inherit from `/etc/manpath`.
export MANPATH="${MANPATH-$(manpath)}:$NPM_PACKAGES/share/man"


#######################################################################
# PATH handling
#######################################################################
#export PATH="$PATH:$HOME/bin:/sbin:$HOME/sw/bin:/opt/pkg/bin:/opt/pkg/sbin"
export PATH="$HOME/sw/bin:$PATH:$HOME/.gem/ruby/2.5.0/bin:$HOME/.cargo/bin"
PATH=$(echo "$PATH" | awk -v RS=':' -v ORS=":" \
	'!a[$1]++{if (NR > 1) printf ORS; printf $a[$1]}')


#######################################################################
# personal prompt
#prompt_time="\[\033[1;33m\]\t\[\033[0m\]"
#prompt_user="\[\033[1;33m\]\u\[\033[0m\]"
#prompt_at="\[\033[1;31m\]@\[\033[0m\]"
#prompt_host="\[\033[1;31m\]\h\[\033[0m\]"
#prompt_cwd="\[\033[1;33m\]\W\[\033[0m\]"
#prompt_last="\[\033[1;35m\]$\[\033[0m\] "
#export PS1="$prompt_user$prompt_at$prompt_host $prompt_cwd $prompt_last"
#export PS1="$prompt_time $PS1"

export PS1="\[\033[1;33m\]\t\[\033[0m\] \[\033[01;32m\]\u@\h\[\033[01;34m\] \W \$\[\033[00m\] "

## terminal window title setting
case ${TERM} in
  xterm*|rxvt*|alacritty)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
    ;;
  screen*)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
    ;;
esac

#######################################################################
# X11 server connection
#######################################################################
if [ -n "`pidof X`" ]; then
	setxkbmap -layout us -option ctrl:nocaps
fi


#######################################################################
# locales
#######################################################################
export LANG="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_NUMERIC="C"
export LC_TIME="C"
export LC_COLLATE="C"
export LC_MONETARY="C"
export LC_MESSAGES="C"
export LC_PAPER="C"
export LC_NAME="C"
export LC_ADDRESS="C"
export LC_TELEPHONE="C"
export LC_MEASUREMENT="C"
export LC_IDENTIFICATION="C"
export LC_ALL=


