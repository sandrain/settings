#!/usr/bin/env bash

# polybar
$HOME/.config/polybar/launch.sh &

# compositor
picom --daemon

# background
$HOME/sw/bin/setbg.sh
#feh --bg-fill ~/Pictures/wallpapers/hyogi/left.png ~/Pictures/wallpapers/hyogi/right.png

# keymapping (removing caps) and lefty mouse
xmodmap ~/.xmodmap
setxkbmap -option caps:ctrl_modifier

# others..
#xset s off -dpms

# fcitx
killall fcitx
fcitx -d -r

# start the terminal
#[[ -z "$(pidof tilda)" ]] && { tilda & }

# signal
[[ -z "$(pidof signal-desktop)" ]] && signal &

# flameshot (screen capture)
flameshot

