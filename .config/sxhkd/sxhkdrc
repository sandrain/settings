#  ------ programs  -------- #

super + Return
    kitty
# alacritty

super + space
    rofi -show run -modi drun,run -columns 2

super + w
    rofi -show window -modi windowcd,window

super + shift + n
    rofi -show filebrowser -modi filebrowser -theme-str 'listview \{lines:12;\}'

# reload config (bspwmrc and sxhkdrc)  #

super + shift + r
    pkill -USR1 -x sxhkd

super + shift + e
    bspc wm -r

super + z
    setxkbmap -layout us -option ctrl:nocaps

#super + q
ctrl + alt + l
    $HOME/.config/polybar/scripts/powercontrol.sh &

ctrl + alt + d
    $HOME/sw/bin/dpms.sh

#  ------ brightness  -------- #

XF86MonBrightnessUp
    xbacklight -inc 10

XF86MonBrightnessDown
    xbacklight -dec 10

#  ------ audio  -------- #

# Fn + s
XF86AudioLowerVolume
    pactl set-sink-volume @DEFAULT_SINK@ -2%

# Fn + d
XF86AudioRaiseVolume
    pactl set-sink-volume @DEFAULT_SINK@ +2%

# Fn + f
XF86AudioMute
    pactl set-sink-mute @DEFAULT_SINK@ toggle

# Fn + q
XF86AudioPrev
    pactl set-source-volume @DEFAULT_SOURCE@ -5% && polybar-msg.sh left pulsemic 1

# Fn + w
XF86AudioPlay
    pactl set-source-mute @DEFAULT_SOURCE@ toggle && polybar-msg.sh left pulsemic 1

# Fn + e
XF86AudioNext
    pactl set-source-volume @DEFAULT_SOURCE@ +5%  && polybar-msg.sh left pulsemic 1


#  ------------ bspwm hotkeys ------------ #

# quit/restart bspwm
super + alt + {q,r}
    bspc {quit,wm -r}

# close and kill
super + {_,shift + }q
    bspc node -{c,k}

# alternate between the tiled and monocle layout
super + Tab
    bspc desktop -l next

# send the newest marked node to the newest preselected node
super + y
    bspc node newest.marked.local -n newest.!automatic.local

# swap the current node and the biggest node
#super + g
#	bspc node -s biggest

# Focus a floating window / Cycle floating windows.
super + {_,shift + }g
    bspc node 'focused.floating#{next,prev}.local.!hidden.floating' -f \
        || bspc node 'last.local.!focused.!hidden.floating' -f \
        || bspc node  'any.local.!focused.!hidden.floating' -f
# Focus a (pseudo_)tiled window / Cycle (pseudo_)tiled windows.
super + {_,shift + }c
    bspc node 'focused.!floating.!fullscreen.window#{next,prev}.local.!hidden.!floating.!fullscreen.window' -f \
        || bspc node 'last.local.!focused.!hidden.!floating.!fullscreen.window' -f \
        || bspc node  'any.local.!focused.!hidden.!floating.!fullscreen.window' -f

# state/flags

# set the window state
super + {d,shift + d,s,f}
    bspc node -t {tiled,pseudo_tiled,floating,fullscreen}

# set the node flags
super + ctrl + {m,x,y,z}
    bspc node -g {marked,locked,sticky,private}

#super + ctrl + {h,j,k,l,0}
#    bspc node -p {west,south,north,east,cancel}

# focus/swap

# focus the node in the given direction
super + {_,shift + }{h,j,k,l}
    bspc node -{f,s} {west,south,north,east}

# focus the node for the given path jump
super + {p,b,comma,period}
    bspc node -f @{parent,brother,first,second}

# focus the next/previous node in the current desktop
#super + {_,shift + }c
#	bspc node -f {next,prev}.local

# focus the next/previous desktop in the current monitor
super + bracket{left,right}
    bspc desktop -f {prev,next}.local

# focus the last node/desktop
super + {grave,Tab}
    bspc {node,desktop} -f last

# focus the older or newer node in the focus history
super + {o,i}
    bspc wm -h off; \
    bspc node {older,newer} -f; \
    bspc wm -h on

# focus or send to the given desktop
super + {_,shift + ctrl + }{1-9,0}
    bspc {desktop -f,node -d} '^{1-9,10}'

# preselect ( select the direction you want the window to be opened in )

# preselect the direction
super + ctrl + {h,j,k,l}
    bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
    bspc node -o 0.{1-9}

# cancel the preselection for the focused node
super + ctrl + space
    bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
    bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

# move/resize

# expand a window by moving one of its side outward
super + alt + {h,j,k,l}
    bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
super + alt + shift + {h,j,k,l}
    bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
super + {Left,Down,Up,Right}
    bspc node -v {-20 0,0 20,0 -20,20 0}

#super + m
#  bspc desktop -l next  # idk what this does


# -------------- take screenshots ------------- #


#fullscreen screenshot save

#super + Print
super + shift + 4
    flameshot gui

#super + Print
#     maim "/home/$USER/Pictures/$(date)"

##cropped save
#super + shift + x
#      maim --select "/home/$USER/Pictures/$(date)"
#
##  clipboard fullscreen
#super + u
#     maim | xclip -selection clipboard -t image/png
#
## clipboard cropped
#super + shift + u
#    maim --select | xclip -selection clipboard -t image/png

# -------------- open applications (custom) ------------- #
#

super + n ; {e,w,m,s,t}
    {thunar,microsoft-edge-stable,thunderbird-bin,signal-desktop,mousepad}
#    {thunar,firefox-bin,thunderbird-bin}

ctrl + alt + v
    $HOME/sw/bin/ddnvpn.sh toggle &

ctrl + alt + m
    $HOME/sw/bin/lofi.sh

ctrl + alt + b
    $HOME/sw/bin/background.sh

