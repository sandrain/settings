"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" basic key bindings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader = ","

set timeoutlen=300
nnoremap <Leader>sv :source $MYVIMRC<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" indentation/whitespaces
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax on
filetype indent on

set nu
set ai
set si
set wrap
set nolist

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

nnoremap <Leader>t2 :set ts=2<CR>:set sw=2<CR>:set sts=2<CR>:set expandtab<CR>
nnoremap <Leader>t4 :set ts=4<CR>:set sw=4<CR>:set sts=4<CR>:set expandtab<CR>
nnoremap <Leader>t8 :set ts=8<CR>:set sw=8<CR>:set sts=8<CR>
nnoremap <Leader>sp :set paste<CR>
nnoremap <Leader>sn :set nopaste<CR>

function! DeleteTrailingWhites()
 :%s/\s\{1,\}$//
 :w
endfunction
nnoremap <Leader>td :call DeleteTrailingWhites()<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.vim/plugged')
Plug 'preservim/nerdtree'
Plug 'rbgrouleff/bclose.vim'
Plug 'skywind3000/asyncrun.vim'
Plug 'arcticicestudio/nord-vim'
Plug 'arithran/vim-delete-hidden-buffers'
Plug 'vim-scripts/DoxygenToolkit.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'rhysd/vim-clang-format', {'for' : ['c', 'cpp']}
Plug 'rust-lang/rust.vim'
Plug 'untitled-ai/jupyter_ascending.vim'
"Plug 'jackguo380/vim-lsp-cxx-highlight'
"Plug 'neoclide/coc.nvim', {'branch':'release'}
call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Buffer management
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <F1> :bprevious<CR>
nnoremap <F2> :bnext<CR>
nnoremap <F4> :Bclose<CR>
nnoremap <F5> :call DeleteHiddenBuffers()<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" color/theme
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set cursorline
set background=dark
colorscheme nord

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" nerdtree
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"nerdtree
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
"let g:NERDTreeShowHidden=1
"let g:NERDTreeSortHiddenFirst=1
nnoremap te :NERDTree<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" status bar (airline)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set laststatus=2
let g:airline_theme='nord'
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
"airline symbols
let g:airline_symbols.paste = '∥'
"let g:airline_symbols.whitespace = 'Ξ'
let g:airline_symbols.whitespace = ' '
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
"let g:airline_symbols.linenr = ''
let g:airline_symbols.linenr = ' '
let g:airline#extensions#tabline#enabled = 1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" jupyter ascending
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap <space><space>x <Plug>JupyterExecute
nmap <space><space>X <Plug>JupyterExecuteAll


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" coc clangd
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" GoTo code navigation.
"nmap <silent> gd <Plug>(coc-definition)
"nmap <silent> gy <Plug>(coc-type-definition)
"nmap <silent> gi <Plug>(coc-implementation)
"nmap <silent> gr <Plug>(coc-references)

"" Use K to show documentation in preview window.
"nnoremap <silent> K :call <SID>show_documentation()<CR>
"
"function! s:show_documentation()
"  if (index(['vim','help'], &filetype) >= 0)
"    execute 'h '.expand('<cword>')
"  elseif (coc#rpc#ready())
"    call CocActionAsync('doHover')
"  else
"    execute '!' . &keywordprg . " " . expand('<cword>')
"  endif
"endfunction
"
"" Highlight the symbol and its references when holding the cursor.
"autocmd CursorHold * silent call CocActionAsync('highlight')
"
"" Symbol renaming.
"nmap <leader>rn <Plug>(coc-rename)
"
"" Formatting selected code.
"xmap <leader>f  <Plug>(coc-format-selected)
"nmap <leader>f  <Plug>(coc-format-selected)
"
"" Add `:Format` command to format current buffer.
"command! -nargs=0 Format :call CocActionAsync('format')
"
"" Add `:Fold` command to fold current buffer.
"command! -nargs=? Fold :call     CocAction('fold', <f-args>)
"
"" Add `:OR` command for organize imports of the current buffer.
"command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')
"
" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
"set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" clang-format
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" configurator: https://zed0.co.uk/clang-format-configurator/
let g:clang_format#style_options = {
    \ 'BasedOnStyle': 'Microsoft',
    \ 'AccessModifierOffset': '-4',
    \ 'AlignAfterOpenBracket': 'Align',
    \ 'AlignConsecutiveMacros': 'true',
    \ 'AlignConsecutiveAssignments': 'true',
    \ 'AlignConsecutiveDeclarations': 'true',
    \ 'AlignEscapedNewlines': 'Right',
    \ 'AlignOperands': 'true',
    \ 'AlignTrailingComments': 'true',
    \ 'AllowAllArgumentsOnNextLine': 'false',
    \ 'AllowAllConstructorInitializersOnNextLine': 'false',
    \ 'AllowAllParametersOfDeclarationOnNextLine': 'false',
    \ 'AllowShortBlocksOnASingleLine': 'false',
    \ 'AllowShortCaseLabelsOnASingleLine': 'true',
    \ 'AllowShortFunctionsOnASingleLine': 'None',
    \ 'AllowShortIfStatementsOnASingleLine': 'Never',
    \ 'AllowShortLoopsOnASingleLine': 'false',
    \ 'AlwaysBreakAfterReturnType': 'None',
    \ 'AlwaysBreakAfterDefinitionReturnType': 'None',
    \ 'AlwaysBreakBeforeMultilineStrings': 'false',
    \ 'BinPackArguments': 'false',
    \ 'BinPackParameters': 'false',
    \ 'BreakBeforeBraces': 'Allman',
    \ 'BreakBeforeTernaryOperators': 'false',
    \ 'BreakConstructorInitializers': 'BeforeColon',
    \ 'BreakInheritanceList': 'BeforeColon',
    \ 'BreakStringLiterals': 'true',
    \ 'ColumnLimit': '100',
    \ 'CompactNamespaces': 'false',
    \ 'ConstructorInitializerAllOnOneLineOrOnePerLine': 'true',
    \ 'ConstructorInitializerIndentWidth': '4',
    \ 'ContinuationIndentWidth': '4',
    \ 'Cpp11BracedListStyle': 'true',
    \ 'DerivePointerAlignment': 'false',
    \ 'FixNamespaceComments': 'true',
    \ 'IncludeBlocks': 'Preserve',
    \ 'IndentCaseLabels': 'false',
    \ 'IndentPPDirectives': 'AfterHash',
    \ 'IndentWidth': '4',
    \ 'KeepEmptyLinesAtTheStartOfBlocks': 'false',
    \ 'Language': 'Cpp',
    \ 'MaxEmptyLinesToKeep': '1',
    \ 'NamespaceIndentation': 'None',
    \ 'PointerAlignment': 'Right',
    \ 'ReflowComments': 'true',
    \ 'SortIncludes': 'false',
    \ 'SortUsingDeclarations': 'false',
    \ 'SpaceAfterCStyleCast': 'true',
    \ 'SpaceAfterLogicalNot': 'false',
    \ 'SpaceAfterTemplateKeyword': 'false',
    \ 'SpaceBeforeAssignmentOperators': 'true',
    \ 'SpaceBeforeCpp11BracedList': 'true',
    \ 'SpaceBeforeCtorInitializerColon': 'true',
    \ 'SpaceBeforeInheritanceColon': 'true',
    \ 'SpaceBeforeParens': 'ControlStatements',
    \ 'SpaceBeforeRangeBasedForLoopColon': 'false',
    \ 'SpaceInEmptyParentheses': 'false',
    \ 'SpacesInAngles': 'false',
    \ 'SpacesInCStyleCastParentheses': 'false',
    \ 'SpacesInContainerLiterals': 'false',
    \ 'SpacesInParentheses': 'false',
    \ 'SpacesInSquareBrackets': 'false',
    \ 'Standard': 'Cpp11',
    \ 'TabWidth': '4',
    \ 'UseTab': 'Never'}

" map to <Leader>cf in C++ code
autocmd FileType c,cpp,objc nnoremap <buffer><Leader>cf :<C-u>ClangFormat<CR>
autocmd FileType c,cpp,objc vnoremap <buffer><Leader>cf :ClangFormat<CR>

" Toggle auto formatting:
let g:clang_format#auto_format=0
"nmap <Leader>C :ClangFormatAutoToggle<CR>
"
" RustFmt
nnoremap <Leader>rf :RustFmt<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" RED license
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! RedCopyright()
 :0r ~/red/share/copyright.txt
 :%s/2012-2020/2012-2022/
 :%s/DDN_FILENAME/\=expand('%:t')/
 :%s/DDN_DATE/\=strftime('%F')/
 :%s/DDN_AUTHOR/Hyogi Sim (hsim@ddn.com)/
 :%s/\s\{1,\}$//
endfunction

":command! Redlicense call RedCopyright()
nnoremap <Leader>rl :call RedCopyright()<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" cscope
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set csto=0
set cst
set nocsverb
set cscopequickfix=s-,c-,d-,i-,t-,e-

nnoremap <F3>s :cs find s <C-R>=expand("<cword>")<CR><CR>
nnoremap <F3>g :cs find g <C-R>=expand("<cword>")<CR><CR>
nnoremap <F3>c :cs find c <C-R>=expand("<cword>")<CR><CR>
nnoremap <F3>t :cs find t <C-R>=expand("<cword>")<CR><CR>
nnoremap <F3>e :cs find e <C-R>=expand("<cword>")<CR><CR>
nnoremap <F3>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
nnoremap <F3>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
nnoremap <F3>d :cs find d <C-R>=expand("<cword>")<CR><CR>
nnoremap <F3>a :cs find a <C-R>=expand("<cword>")<CR><CR>

augroup qf
  autocmd!
  autocmd QuickFixCmdPost * cwindow
augroup END

"Using ctag
set tagbsearch
set tags=./tags
let Tlist_Sort_Type="name"

if filereadable("./cscope.out")
  cs add cscope.out
endif

"AsyncRun
let g:asyncrun_open = 8

nnoremap <Leader>M :AsyncRun make<CR>
nnoremap <Leader>R :AsyncRun ./mktags.sh<CR>
nnoremap <Leader>r :cscope reset<CR>
nnoremap <Leader>, :cclose<CR>

set csverb

"Using man page
func! Man()
  let sm = expand("<cword>")
  exe "!man -S 2:3:4:5:6:7:8:9:tcl:n:l:p:o ".sm
endfunc
nnoremap <Leader>ma :call Man()<CR><CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" python PEP 7 & 8
" (from http://svn.python.org/projects/python/trunk/Misc/Vim/vimrc)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
au BufRead,BufNewFile *py,*pyw,*.c,*.h set tabstop=4
au BufRead,BufNewFile *.py,*pyw set shiftwidth=4
au BufRead,BufNewFile *.py,*.pyw set expandtab
fu Select_c_style()
    if search('^\t', 'n', 150)
        set shiftwidth=4
        set noexpandtab
    el
        set shiftwidth=4
        set expandtab
    en
endf
au BufRead,BufNewFile *.c,*.h call Select_c_style()
au BufRead,BufNewFile Makefile* set noexpandtab
highlight BadWhitespace ctermbg=red guibg=red
au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h set textwidth=79
au BufRead,BufNewFile *.c,*.h set formatoptions-=c formatoptions-=o formatoptions-=r
au BufNewFile *.py,*.pyw,*.c,*.h set fileformat=unix
let python_highlight_all=1
syntax on

