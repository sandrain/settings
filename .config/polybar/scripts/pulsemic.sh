#!/usr/bin/env bash

cmd="$1"

if [ "$cmd" == "toggle" ]; then
  pactl set-source-mute @DEFAULT_SOURCE@ toggle
fi

outstr=""

## is this muted?
mute=$(pactl get-source-mute @DEFAULT_SOURCE@ | awk '{print $NF}')
if [ "$mute" == "yes" ]; then
  outstr+="%{F#bf616a} %{F-}"
else
  outstr+=" "
fi

## get the current volume
vol=$(pactl get-source-volume @DEFAULT_SOURCE@ | head -n1 | awk '{print $5}')
outstr+="$vol"

echo $outstr

exit 0

