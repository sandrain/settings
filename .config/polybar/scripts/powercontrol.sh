#!/usr/bin/env bash

uptime="$(uptime -p | sed -e 's/up //g')"
dpms="$(xset -q | grep DPMS | tail -n1)"

lock=" Lock"
suspend="⏾ Sleep"
logout=" Logout"

confirm_exit() {
  echo "Yes|Cancel" | \
    rofi -sep '|' -dmenu \
         -theme-str 'window {width:30%;} listview {lines:2;}' -p "$1 Are You Sure?"
}

options="$lock\n$suspend\n$logout"
status="System ($(hostname)) has been up for ${uptime}. ${dpms}."

chosen="$(echo -e "$options" | \
          rofi -p "$status" -dmenu -theme-str "listview {lines:3;} window {width:30%;}" \
               -selected-row 0)"

revert() {
  # picom is currently broken.. i3lock and it will crash
  pidof picom || picom --daemon
}

case $chosen in
  $lock)
    trap revert HUP INT TERM
    #i3lock -e -c 000000 && xset dpms force standby
    xset dpms force standby
    i3lock -n -e -c 000000
    revert
    ;;
  $suspend)
    # suspend not stable with the current nvidia driver + msedge (or chrome)
    i3lock -e -c 000000 && loginctl suspend
    ;;
  $logout)
    ans=$(confirm_exit "Logout the session." &)
    if [[ $ans == "Yes" ]]; then
      bspc quit
    else
      exit 0
    fi
    ;;
esac

