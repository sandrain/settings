#!/usr/bin/env bash

current_status="$(xset -q | grep DPMS | tail -n1 | awk '{print $NF}')"
primarycolor="$(grep ^primary $HOME/.config/polybar/colors.ini | awk '{print $NF}')"

if [ "$current_status" == "Disabled" ]; then
  echo "%{F${primarycolor}}%{F-}"
else
  echo ""
fi

exit 0

