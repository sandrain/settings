#!/usr/bin/env bash

pidfile="/tmp/lofi.ffplay.pid"

primarycolor="$(grep ^primary $HOME/.config/polybar/colors.ini | awk '{print $NF}')"
defaultcolor="$(grep ^gray $HOME/.config/polybar/colors.ini | awk '{print $NF}')"

if [ -f "$pidfile" ]; then
  station="$(head -n1 $pidfile) .."
  echo "%{F${primarycolor}}$station%{F-}"
else
  msg="ﱘ Playing silence .."
  echo "%{F${defaultcolor}}$msg%{F-}"
fi

