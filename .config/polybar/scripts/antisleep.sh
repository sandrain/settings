#!/usr/bin/env bash

current_status="$(xset -q | grep DPMS | tail -n1 | awk '{print $NF}')"

if [ "$current_status" == "Disabled" ]; then
  echo "%{F#ef5350}%{F-}"
else
  echo ""
fi

exit 0

