#!/bin/sh

get_icon() {
  case $1 in
    # Icons for weather-icons
    01d) icon="";;
    01n) icon="";;
    02d) icon="";;
    02n) icon="";;
    03*) icon="";;
    04*) icon="";;
    09d) icon="";;
    09n) icon="";;
    10d) icon="";;
    10n) icon="";;
    11d) icon="";;
    11n) icon="";;
    13d) icon="";;
    13n) icon="";;
    50d) icon="";;
    50n) icon="";;
    *) icon="";

    # Icons for Font Awesome 5 Pro
    #01d) icon="";;
    #01n) icon="";;
    #02d) icon="";;
    #02n) icon="";;
    #03d) icon="";;
    #03n) icon="";;
    #04*) icon="";;
    #09*) icon="";;
    #10d) icon="";;
    #10n) icon="";;
    #11*) icon="";;
    #13*) icon="";;
    #50*) icon="";;
    #*) icon="";
  esac

  echo $icon
}

function deg2dir() {
  deg=$1
  if [ $deg -ge 348 ] && [ $deg -lt 11 ]; then echo N; fi
  if [ $deg -ge 11 ] && [ $deg -lt 33 ]; then echo NNE; fi
  if [ $deg -ge 33 ] && [ $deg -lt 56 ]; then echo NE; fi
  if [ $deg -ge 56 ] && [ $deg -lt 78 ]; then echo ENE; fi
  if [ $deg -ge 78 ] && [ $deg -lt 101 ]; then echo E; fi
  if [ $deg -ge 101 ] && [ $deg -lt 123 ]; then echo ESE; fi
  if [ $deg -ge 123 ] && [ $deg -lt 146 ]; then echo SE; fi
  if [ $deg -ge 146 ] && [ $deg -lt 168 ]; then echo SSE; fi
  if [ $deg -ge 168 ] && [ $deg -lt 191 ]; then echo S; fi
  if [ $deg -ge 191 ] && [ $deg -lt 213 ]; then echo SSW; fi
  if [ $deg -ge 213 ] && [ $deg -lt 236 ]; then echo SW; fi
  if [ $deg -ge 236 ] && [ $deg -lt 258 ]; then echo WSW; fi
  if [ $deg -ge 258 ] && [ $deg -lt 281 ]; then echo W; fi
  if [ $deg -ge 281 ] && [ $deg -lt 303 ]; then echo WNW; fi
  if [ $deg -ge 303 ] && [ $deg -lt 326 ]; then echo NW; fi
  if [ $deg -ge 326 ] && [ $deg -lt 348 ]; then echo NNW; fi
}

# wait until internet connection is available
max_wait=10
cnt=0

while [ $cnt -le $max_wait ]; do
  wget -q --spider http://google.com
  if [ $? -eq 0 ]; then
    break
  else
    sleep 1
    ((cnt++))
  fi
done

KEY="22de0e0b5f94f421be185e4a5d3c8a05"
CITY="4482941" # oak ridge
ZIP="78717"
UNITS="imperial"
SYMBOL="°"

API="https://api.openweathermap.org/data/2.5"

weather=$(curl -sf "$API/weather?appid=$KEY&zip=$ZIP&units=$UNITS")

primarycolor="$(grep ^primary $HOME/.config/polybar/colors.ini | awk '{print $NF}')"
defaultcolor="$(grep ^cyan $HOME/.config/polybar/colors.ini | awk '{print $NF}')"

if [ -n "$weather" ]; then
  icon=$(echo "$weather" | jq -r ".weather[0].icon")
  main=$(echo "$weather" | jq ".weather[0].main" | sed 's/"//g')
  temp=$(echo "$weather" | jq ".main.temp" | cut -d "." -f 1)$SYMBOL
  min=$(echo "$weather" | jq ".main.temp_min" | cut -d "." -f 1)$SYMBOL
  max=$(echo "$weather" | jq ".main.temp_max" | cut -d "." -f 1)$SYMBOL
  feel=$(echo "$weather" | jq ".main.feels_like" | cut -d "." -f 1)$SYMBOL
  humidity=$(echo "$weather" | jq ".main.humidity" | cut -d "." -f 1)"%"
  wind=$(echo "$weather" | jq ".wind.speed" | cut -d "." -f 1)" mph"
  deg=$(echo "$weather" | jq ".wind.deg" | cut -d "." -f 1)
  dir=$(deg2dir $deg)


  res="%{F${primarycolor}}$(get_icon "$icon") $temp %{F-} "
  res+="(${main}, feels like $feel): "
  res+="%{F${defaultcolor}} %{F-} $min-$max "
  res+="%{F${defaultcolor}} %{F-} $humidity "
  res+="%{F${defaultcolor}} %{F-} $wind $dir"

  echo $res
fi
