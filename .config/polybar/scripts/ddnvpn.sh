#!/usr/bin/env bash

function status() {
  status="Not connected"
  if [ -n "$(pidof openconnect)" ] && [ -f '/etc/resolv.conf.head' ]; then
    status="$(ip addr show dev tun0 | grep inet | awk '{print $2}')"
  fi
  echo $status
}

primarycolor="$(grep ^primary $HOME/.config/polybar/colors.ini | awk '{print $NF}')"
defaultcolor="$(grep ^gray $HOME/.config/polybar/colors.ini | awk '{print $NF}')"

## main
res="$(status)"
if [ "$res" == "Not connected" ]; then
  echo "%{F${defaultcolor}}$res%{F-}"
else
  echo "%{F${primarycolor}}$res%{F-}"
fi

exit 0

