#!/usr/bin/env bash

dir=$HOME/Pictures/wallpapers/hyogi
files=($(ls $dir))

filelist=$(for f in ${files[@]}; do echo $f; done | paste -sd '|')

msg="Select a background:"
ans=$(echo "$filelist" | rofi -sep '|' -dmenu -width 20 -p "$msg")

feh --bg-fill $dir/$ans

exit 0
