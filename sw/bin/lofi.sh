#!/bin/bash

pidfile="/tmp/lofi.ffplay.pid"

station_titles=(
  " lofi hip hop radio - beats to RELAX/STUDY to"
  " lofi hip hop radio - beats to SLEEP/CHILL to"
  " coffee chop radio // 24/7 lofi hip-hop beats"
  " The Good Life Radio • 24/7 Live Radio Best Relax House, Chillout, Study, Running, Gym, Happy Music"
  " lofi hip hop radio - sad & sleepy beats"
  " Radio 70's Live - The Best of Funky Soul Disco Hits"
  " Night Paris JAZZ - Slow Sax Jazz Music - Relaxing Background Music"
  " Bossa Nova Covers 2021 - Cool Music"
)

station_ids=(
  "5qap5aO4i9A"
  "DWcJFNfaw9c"
  "-5KAN9_CzSA"
  "36YnV9STBqc"
  "l7TxwBhtTUY"
  "kxf5P0ejS9s"
  "QN1uygzp56s"
  "tEpK1F0GGzk"
)

i=0
len=${#station_titles[@]}
station_list=""
for t in "${station_titles[@]}"; do
  station_list+="$t"
  if [ $i -lt $((len-1)) ]; then
    station_list+="|"
  fi
  ((i++))
done

function play_station() {
  url="https://www.youtube.com/watch?v=${station_ids[$1]}"

  youtube-dl -q -f 96 $url -o - | ffplay -nodisp -autoexit -loglevel quiet -i - &
  sleep 1
  echo ${station_titles[$1]} > $pidfile
  echo $(pidof ffplay) >> $pidfile
}

function stop_radio() {
  kill -15 $(pidof ffplay)
  rm -f $pidfile
}

function tune_station() {
  msg="Select a station to play:"
  ans=$(echo "$station_list" | \
        rofi -theme-str "listview {columns:1;lines:$len;} window {width:40%;}" \
        -sep '|' -dmenu -p "$msg")

  for (( i=0; i < ${#station_titles[@]}; i++ )); do
    if [ "$ans" == "${station_titles[$i]}" ]; then
      stop_radio
      play_station $i
      break
    fi
  done
}

if [ -f $pidfile ]; then
  station=$(head -n1 $pidfile)
  msg="Currently playing .. $station .."
  ans=$(echo "Stop Playing|Change Station|Cancel" | \
        rofi -sep '|' -dmenu -theme-str "window {width:30%;} listview {lines:3;}" -p "$msg")

  if [[ "$ans" == "Stop Playing" ]]; then
    stop_radio
  elif [[ "$ans" == "Change Station" ]]; then
    tune_station
  else
    exit 0
  fi
else
  tune_station
fi

$HOME/sw/bin/polybar-msg.sh left lofi 1

