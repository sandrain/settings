#!/usr/bin/env bash

session="logs"

tmux new-session -d -s $session
tmux rename-window -t 1 'syslog'
tmux split-window -v -p 50
tmux send-keys -t $session:'syslog'.0 'tail -f /var/log/messages' C-m
tmux send-keys -t $session:'syslog'.1 'tail -f /var/log/syslog' C-m

tmux new-window -t $session:2 -n 'kernel'
tmux send-keys -t $session:'kernel'.0 'dmesg -w' C-m

tmux -2 attach-session -t $session:2

#tmux new -s tt \; \
#  send-keys 'tail -f /var/log/messages' C-m \; \
#  split-window -v -p 50 \; \
#  send-keys 'tail -f /var/log/syslog' C-m \; \
#  select-pane -t 1 \; \
#  rename-window 'syslog' \;

