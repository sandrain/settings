#!/usr/bin/env bash

vpn_locations=(MD CO)

declare -A vpn_url
vpn_url[MD]="https://ddnmobile-mar.datadirectnet.com/network"
vpn_url[CO]="https://cos.vpn.ddn.com/network"

function selectvpn() {
  msg="Select VPN to connect"
  ops=""
  for lo in "${vpn_locations[@]}"; do
    ops+="$lo (${vpn_url[$lo]})\n"
  done

  action="$(echo -e "$ops" | \
            rofi -p "$msg" -dmenu \
                 -theme-str "window {width:30%;} listview {columns:1;lines:2;}" \
                 -selected-row 0)"
  for lo in "${vpn_locations[@]}"; do
    if [ "$action" == "$lo (${vpn_url[$lo]})" ]; then
      echo "$lo"
      return 0
    fi
  done

  exit 1
}

function connect() {
  url="${vpn_url[$1]}"
	echo "wth83Zsh(*)A!" | \
    sudo openconnect --user=hsim --protocol=pulse \
                     --background --syslog $url
  if [ $? -ne 0 ]; then
    rofi -e "Failed to connect to VPN"
    exit 1
  fi

  cat << 'EOF' | sudo tee /etc/resolv.conf.head
nameserver 127.0.0.53
options edns0 trust-ad
search datadirect.datadirectnet.com red.datadirectnet.com datadirectnet.com
nameserver 10.52.96.48
nameserver 10.52.96.49
EOF

  feh --bg-fill $HOME/Pictures/wallpapers/hyogi/blackandlight.png \
                $HOME/Pictures/wallpapers/hyogi/blackandlight.png
  sleep 1

  rofi -e "$(ifconfig tun0)"
  return 0
}

function disconnect() {
  sudo killall openconnect
  sudo rm -f /etc/resolv.conf.head

  sleep 1
  feh --bg-fill $HOME/Pictures/wallpapers/hyogi/blackandlight.png \
                $HOME/Pictures/wallpapers/hyogi/baby01.png
}

function status() {
  status="Not connected"
  if [ -n "$(pidof openconnect)" ] && [ -f '/etc/resolv.conf.head' ]; then
    status="$(ip addr show dev tun0 | grep inet | awk '{print $2}')"
  fi
  echo $status
}

function toggle() {
  connect="Connect"
  disconnect="Disconnect"
  cleanup="Cleanup"
  cancel="Cancel"

  options=""

  msg=" DDN VPN: "

  if [ -n "$(pidof openconnect)" ] && [ -f /etc/resolv.conf.head ]; then
    options="$disconnect\n$cancel"
    msg+="connected (ip addr=$(status))." 
  else
    options="$connect\n$cleanup\n$cancel"
    msg+="not connected."
  fi

  action="$(echo -e "$options" | \
            rofi -p "$msg" -dmenu \
                 -theme-str "window {width:30%;} listview {columns:1;lines:3;}" \
                 -selected-row 0)"
  case "$action" in
    $connect)
      vpn="$(selectvpn)"
      if [ $? -ne 0 ]; then
        exit 1
      fi
      connect $vpn
      ;;

    $disconnect)
      disconnect
      ;;

    $cleanup)
      disconnect
      ;;

    *)
      ;;
  esac
}


## main

cmd="$1"

case "$cmd" in
  "toggle")
    $cmd
    ;;
  *)
    ;;
esac

$HOME/sw/bin/polybar-msg.sh right ddnvpn 1

exit 0

