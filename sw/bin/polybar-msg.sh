#!/usr/bin/env bash
# usage: polybar-msg.sh <left|right> <module name> <hook #>

if [ $# -ne 3 ]; then
  echo "usage: $0 <left|right> <module> <hook #>"
  exit 1
fi

bar="$1"
module="$2"
hook="$3"

pid=$(pgrep -f "polybar.*${bar}.ini")
if [ -z "$pid" ]; then
  echo "cannot find polybar process id"
  exit 1
fi

polybar-msg -p $pid hook $module $hook

