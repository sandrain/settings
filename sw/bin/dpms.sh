#!/usr/bin/env bash

# see if we have valid argument of 'on' or 'off'
# otherwise toggle the current status
arg="$1"
current_status="$(xset -q | grep 'DPMS is' | awk '{print $NF}')"

case $arg in
  on)
    mode="on"
    ;;
  off)
    mode="off"
    ;;
  *)
    if [ "$current_status" == "Enabled" ]; then
      mode="off"
    else
      mode="on"
    fi
    ;;
esac

echo $mode

if [ "$mode" == "on" ]; then
  xset s on +dpms
else
  xset s off -dpms
fi

$HOME/sw/bin/polybar-msg.sh right dpms 1

