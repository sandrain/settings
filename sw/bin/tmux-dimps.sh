#!/usr/bin/env bash

session="dimps"

# if already running, attach the existing
tmp=$(tmux list-sessions -F '#{session_name}' | grep "^$session")
if [ -n "$tmp" ]; then
  echo "session exists"
  tmux attach-session -d -t $session
  exit 0
fi

# sys window
tmux new-session -d -s $session
tmux rename-window -t 1 'sys'
tmux split-window -h -p 60
tmux split-window -h -p 45
tmux split-window -v -p 91
tmux send-keys -t $session:'sys'.0 'runmc' C-m
tmux send-keys -t $session:'sys'.1 'neofetch' C-m
tmux send-keys -t $session:'sys'.2 'runcava' C-m
tmux send-keys -t $session:'sys'.3 'btop' C-m

# gentoo window
tmux new-window -t $session:2 -n 'gentoo'
tmux split-window -h -p 65 -t $session:2
tmux split-window -h -p 50 -t $session:2
tmux split-window -v -p 50 -t $session:2
tmux send-keys -t $session:'gentoo'.0 'emerge --info' C-m
tmux send-keys -t $session:'gentoo'.2 'dmesg -w' C-m
tmux send-keys -t $session:'gentoo'.3 'tail -f /var/log/messages' C-m

# attach
tmux -2 attach-session -t $session:1 \; \
        send-keys -t $session:'sys'.3 'h' C-m \; \
        send-keys -t $session:'sys'.3 'h' C-m \; \
        select-pane -t $session:'sys'.1

#tmux new -s dimps \; \
#  send-keys 'runcava' C-m \; \
#  split-window -v -p 91 \; \
#  send-keys 'runmc' C-m \; \
#  split-window -h -p 60 \; \
#  send-keys 'neofetch' C-m \; \
#  split-window -h -p 50 \; \
#  send-keys 'btop' C-m \; \
#  select-pane -t 1 \; \
#  rename-window 'sys' \;


