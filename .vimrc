set nu
set ai
set si

syntax enable
set wrap
"set linebreak
set nolist

set background=dark
colorscheme nord
"colorscheme solarized
"let g:solarized_termtrans=1
"let g:solarized_bold=1
"let g:solarized_contrast="normal"
"let g:solarized_visibility="normal"


				
set cursorline

syntax on
set t_Co=256

"set t_AB=^[[48;5;%dm
"set t_AF=^[[38;5;%dm

set textwidth=0 wrapmargin=0
"highlight OverLength ctermbg=red ctermfg=white
"match OverLength /\%81v.\+/
"set textwidth=80
set laststatus=2
set statusline=\(%n\)%<%f\ %h%m%r%=0x%B\ \ \ \ %-14.(%l,%c%V%)\ %P

noremap <silent> <F11> :cal VimCommanderToggle()<CR>

"Using cscope
"set csprg=
set csto=0
set cst
set nocsverb

set cscopequickfix=s-,c-,d-,i-,t-,e-

nmap <F3>s :cs find s <C-R>=expand("<cword>")<CR><CR>
nmap <F3>g :cs find g <C-R>=expand("<cword>")<CR><CR>
nmap <F3>c :cs find c <C-R>=expand("<cword>")<CR><CR>
nmap <F3>t :cs find t <C-R>=expand("<cword>")<CR><CR>
nmap <F3>e :cs find e <C-R>=expand("<cword>")<CR><CR>
nmap <F3>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
nmap <F3>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
nmap <F3>d :cs find d <C-R>=expand("<cword>")<CR><CR>
nmap <F3>a :cs find a <C-R>=expand("<cword>")<CR><CR>

"Using ctag
set tagbsearch
set tags=./tags
let Tlist_Sort_Type="name"

if filereadable("./cscope.out")
  cs add cscope.out
endif

"AsyncRun
let g:asyncrun_open = 8
map mm :AsyncRun make<CR>
map ,, :cclose<CR>

set csverb
"set nowrap

"Using man page
func! Man()
  let sm = expand("<cword>")
  exe "!man -S 2:3:4:5:6:7:8:9:tcl:n:l:p:o ".sm
endfunc
nmap ,ma :call Man()<cr><cr>

"Spell
"autocmd ColorScheme * hi clear SpellBad \| hi SpellBad cterm=underline,bold ctermfg=white ctermbg=black
"autocmd ColorScheme * hi clear SpellBad \| hi SpellBad cterm=underline,bold
autocmd BufRead,BufNewFile *.tex set spell
autocmd BufRead,BufNewFile *.tex set paste
autocmd BufRead,BufNewFile *.txt set spell

set spelllang=en_us

" Force to use underline for spell check results
augroup SpellUnderline
  autocmd!
  autocmd ColorScheme *
    \ highlight SpellBad
    \   cterm=Underline,Bold
    \   ctermfg=NONE
    \   ctermbg=NONE
    \   term=Reverse
    \   gui=Undercurl
    \   guisp=Red
  autocmd ColorScheme *
    \ highlight SpellCap
    \   cterm=Underline,Bold
    \   ctermfg=NONE
    \   ctermbg=NONE
    \   term=Reverse
    \   gui=Undercurl
    \   guisp=Red
  autocmd ColorScheme *
    \ highlight SpellLocal
    \   cterm=Underline,Bold
    \   ctermfg=NONE
    \   ctermbg=NONE
    \   term=Reverse
    \   gui=Undercurl
    \   guisp=Red
  autocmd ColorScheme *
    \ highlight SpellRare
    \   cterm=Underline,Bold
    \   ctermfg=NONE
    \   ctermbg=NONE
    \   term=Reverse
    \   gui=Undercurl
    \   guisp=Red
  augroup END


"TreeExplore
"let treeExplVertical=1
"let treeExplhidden=1
"let treeExplWinSize=30
""let treeExplHidePattern=".*\.l*o"
"map te :VSTreeExplore<cr>:set nonu<cr>

let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

map te :NERDTree<cr>
"autocmd vimenter * NERDTree

"Fast switching
map t2 :set ts=2<CR>:set sw=2<CR>:set sts=2<CR>:set expandtab<CR>
map t4 :set ts=4<CR>:set sw=4<CR>:set sts=4<CR>:set expandtab<CR>
map t8 :set ts=8<CR>:set sw=8<CR>:set sts=8<CR>
map tp :set paste<CR>

set comments=sl:/*,mb:\ *,elx:\ */

" vimrc file for following the coding standards specified in PEP 7 & 8.
"
" To use this file, source it in your own personal .vimrc file (``source
" <filename>``) or, if you don't have a .vimrc file, you can just symlink to it
" (``ln -s <this file> ~/.vimrc``).  All options are protected by autocmds
" (read below for an explanation of the command) so blind sourcing of this file
" is safe and will not affect your settings for non-Python or non-C files.
"
"
" All setting are protected by 'au' ('autocmd') statements.  Only files ending
" in .py or .pyw will trigger the Python settings while files ending in *.c or
" *.h will trigger the C settings.  This makes the file "safe" in terms of only
" adjusting settings for Python and C files.
"
" Only basic settings needed to enforce the style guidelines are set.
" Some suggested options are listed but commented out at the end of this file.

" Number of spaces that a pre-existing tab is equal to.
" For the amount of space used for a new tab use shiftwidth.
"au BufRead,BufNewFile *py,*pyw,*.c,*.h set tabstop=8
au BufRead,BufNewFile *py,*pyw,*.v,*.java set tabstop=4
au BufRead,BufNewFile *.cc,*.cpp,*.hpp,*.ned,*.sh set tabstop=2

" What to use for an indent.
" This will affect Ctrl-T and 'autoindent'.
" Python: 4 spaces
" C: tabs (pre-existing files) or 4 spaces (new files)
au BufRead,BufNewFile *.h,*.c,*.py,*pyw,*.v,*.java set shiftwidth=4
au BufRead,BufNewFile *.cc,*.cpp,*.hpp,*.ned,*.sh set shiftwidth=2
au BufRead,BufNewFile *.h,*.c,*.py,*.pyw,*.v,*.java,*.cc,*.cpp,*.hpp,*.ned,*.sh set expandtab
fu Select_c_style()
    set tabstop=8
    set softtabstop=8
    set shiftwidth=8
    set noexpandtab
"    if search('^\t', 'n', 150)
"        set shiftwidth=8
"        set noexpandtab
"    el
"        set shiftwidth=4
"        set expandtab
"    en
endf
"au BufRead,BufNewFile *.c,*.h call Select_c_style()
au BufRead,BufNewFile Makefile* set noexpandtab
au BufRead,BufNewFile makefile* set noexpandtab

" Use the below highlight group when displaying bad whitespace is desired.
highlight BadWhitespace ctermbg=red guibg=red

" Display tabs at the beginning of a line in Python mode as bad.
au BufRead,BufNewFile *.h,*.c,*.py,*.pyw,*.v,*.java,*.cc,*.cpp,*.hpp,*.ned,*.sh match BadWhitespace /^\t\+/
" Make trailing whitespace be flagged as bad.
au BufRead,BufNewFile *.h,*.c,*.py,*.pyw,*.v,*.java,*.c,*.h,*.cc,*.cpp,*.hpp,*.ned,*.sh match BadWhitespace /\s\+$/

" Wrap text after a certain number of characters
" Python: 79
" C: 79
"au BufRead,BufNewFile *.py,*.pyw,*.c,*.h set textwidth=79
au BufRead,BufNewFile *.py,*.pyw,*.c set textwidth=79

" Turn off settings in 'formatoptions' relating to comment formatting.
" - c : do not automatically insert the comment leader when wrapping based on
"    'textwidth'
" - o : do not insert the comment leader when using 'o' or 'O' from command mode
" - r : do not insert the comment leader when hitting <Enter> in insert mode
" Python: not needed
" C: prevents insertion of '*' at the beginning of every line in a comment
"au BufRead,BufNewFile *.c,*.h set formatoptions-=c formatoptions-=o formatoptions-=r

" Use UNIX (\n) line endings.
" Only used for new files so as to not force existing files to change their
" line endings.
" Python: yes
" C: yes
au BufNewFile *.py,*.pyw,*.v,*.java,*.c,*.h set fileformat=unix

" clangformat
let g:clang_format#code_style = "google"
let g:clang_format#style_options = {
  \ "IndentWidth": 4,
  \ "AccessModifierOffset": -4,
  \ "AlignAfterOpenBracket": "true",
  \ "AlwaysBreakTemplateDeclarations": "true",
  \ "Standard" : "C++11"}

" map to <Leader>cf in C++ code
"autocmd FileType c,hpp,cc,cpp,objc nnoremap <buffer><Leader>cf :<C-u>ClangFormat<CR>
"autocmd FileType c,hpp,cc,cpp,objc vnoremap <buffer><Leader>cf :ClangFormat<CR>
" Toggle auto formatting:
"nmap <Leader>C :ClangFormatAutoToggle<CR>

map cf :ClangFormat<CR>

" ----------------------------------------------------------------------------
" The following section contains suggested settings.  While in no way required
" to meet coding standards, they are helpful.

" Set the default file encoding to UTF-8: ``set encoding=utf-8``
set encoding=utf-8

" Puts a marker at the beginning of the file to differentiate between UTF and
" UCS encoding (WARNING: can trick shells into thinking a text file is actually
" a binary file when executing the text file): ``set bomb``

" For full syntax highlighting:
"``let python_highlight_all=1``
"``syntax on``
let python_highlight_all=1
syntax on

" Automatically indent based on file type: ``filetype indent on``
" Keep indentation level from previous line: ``set autoindent``
filetype indent on
set autoindent

" Folding based on indentation: ``set foldmethod=indent``

" ----------------------------------------------------------------------------
" Doxygen params.

let g:DoxygenToolkit_licenseTag = "\<enter>Copyright (C) Hyogi Sim <sandrain@gmail.com>\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag .  "---------------------------------------------------------------------------\<enter>"
let g:DoxygenToolkit_licenseTag = g:DoxygenToolkit_licenseTag . "Refer to the LICENSE.txt file for the complete license."

let g:DoxygenToolkit_authorName = "Hyogi Sim <sandrain@gmail.com>"

